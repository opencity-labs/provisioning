#!/usr/bin/env bash
#
# Enable termination protection
# https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/terminating-instances.html#Using_ChangingDisableAPITermination
#
# Required policy:  (name it 'Ec2AllowChangeOfTerminationProtection')
#
#{
#    "Version": "2012-10-17",
#    "Statement": [
#        {
#            "Sid": "VisualEditor0",
#            "Effect": "Allow",
#            "Action": "ec2:ModifyInstanceAttribute",
#            "Resource": "*",
#            "Condition": {
#                "ForAnyValue:StringEquals": {
#                    "ec2:Attribute/disableApiTermination": "true"
#                }
#            }
#        }
#    ]
#}

# Add the policy to the IAM Role of the EC2 host and this script will run smoothly!
#

# set -ex

if [[ -z $HOSTNAME ]]; then
  echo "ERROR, cannot procede without a valid HOSTNAME"
  exit 1
fi

# region=$(http http://169.254.169.254/latest/dynamic/instance-identity/document | jq -c -r .region)
region=$(curl --silent http://169.254.169.254/latest/dynamic/instance-identity/document | grep "region" | awk -F '"' '{print $4}')
if [[ -z $region ]]; then
  echo "ERROR, cannot get region info from metatags, am I an EC2 instance? Are httpie and jq installed?"
  exit 2
fi

# id=$(http http://169.254.169.254/latest/meta-data/instance-id)
id=$(curl --silent http://169.254.169.254/latest/meta-data/instance-id)

if [[ ! -f ~/.aws/config ]]; then
  [[ ! -d ~/.aws ]] && mkdir ~/.aws

  echo -e "[default]\nregion = $region\n" > ~/.aws/config
  echo "* Configured aws-cli with default region '$region'"
fi



echo "* Setting termination protection"
echo
if aws ec2 modify-instance-attribute --instance-id  "$id" --disable-api-termination '{"Value":true}'; then
  echo
else
  echo "ERROR, cannot set termination protectino to instance '$id', is there a policy that allow action it in the instance ROLE?"
  exit 3
fi

echo
echo "==> Finished successfully!"

