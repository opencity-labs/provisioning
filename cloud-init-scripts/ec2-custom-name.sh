#!/usr/bin/env bash
# set hostname to current name with the last 4 chars of instance id
#
id=$(ec2metadata --instance-id)
myname=${HOSTNAME}-${id:(-4)}
hostnamectl set-hostname "$myname"
sed -i "s/${HOSTNAME}/${myname}/g" /etc/hosts
echo "New hostname is ${myname}"
