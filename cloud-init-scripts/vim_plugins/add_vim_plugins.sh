#!/bin/bash

# We inject what is necessary to enable the vim plugin manager: https://github.com/junegunn/vim-plug
# and add the plugin we want
# see https://gitlab.com/opencity-labs/provisioning/

set -ex

# Ubuntu: assure autoload is accessible by all users
mkdir -p /etc/vim/autoload
chmod a+rx /etc/vim/autoload

# Warning:
#   in Fedora 40+ the "systemwise" autoload directory is /usr/share/vim/vimfiles/autoload and not /etc/vim/autoload/
# scarico copia di https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim e metto in /etc/vim/autoload/
curl -fLo /etc/vim/autoload/plug.vim --create-dirs https://gitlab.com/opencity-labs/provisioning/-/raw/main/cloud-init-scripts/vim_plugins/plug.vim

# note: we presume /etc/vim/vimrc.local already include the following
#  if filereadable("/etc/vim/vimrc.plugins.local")
#    source /etc/vim/vimrc.plugins.local
#  endif

# populate /etc/vim/vimrc.plugins.local
curl -fLo /etc/vim/vimrc.plugins.local https://gitlab.com/opencity-labs/provisioning/-/raw/main/cloud-init-scripts/vim_plugins/vimrc.plugins.local

# Now we want to install the plugin
vim -c 'PlugInstall' -c 'qall!'
