#!/usr/bin/env bash

# this_script eipalloc-xxxxxxxxxx
# associate to this instance the Elstic IP eipalloc-xxxxxxxxxx
#
# Note: needs proper role permission
#
# disassociace IP 54.195.130.30 ($EIP) in case is associated with a stale/crashed Ec2

EIP=$1


# region=$(http http://169.254.169.254/latest/dynamic/instance-identity/document | jq -c -r .region)
region=$(curl --silent http://169.254.169.254/latest/dynamic/instance-identity/document | grep "region" | awk -F '"' '{print $4}')
if [[ -z $region ]]; then
  echo "ERROR, cannot get region info from metatags, am I an EC2 instance? Are httpie and jq installed?"
  exit 2
fi

# id=$(http http://169.254.169.254/latest/meta-data/instance-id)
id=$(curl --silent http://169.254.169.254/latest/meta-data/instance-id)

if [[ ! -f ~/.aws/config ]]; then
  [[ ! -d ~/.aws ]] && mkdir ~/.aws

  echo -e "[default]\nregion = $region\n" > ~/.aws/config
  echo "* Configured aws-cli with default region '$region'"
fi

aws ec2 disassociate-address --association-id "$EIP"  || echo "IP is already diassociated"
# associate IP 54.195.130.30 to itself
aws ec2 associate-address --instance-id "$id" --allocation-id "$EIP"
