#!/usr/bin/env bash

if ! which byobu >/dev/null; then
  echo "Byobu not installed, skip configuration"
  exit 1
fi

[[ ! -f /var/lock/.byobu-shell-fixed ]] && \
  echo "fixes byobu problem with UMASK, now byobu-shell is a login shell" && \
  sed -i -e "s/exec \"\$SHELL\"/exec \"\$SHELL\" --login/" /usr/bin/byobu-shell && \
  touch /var/lock/.byobu-shell-fixed

echo "Launcher install"
/usr/bin/byobu-launcher-install

echo "Selected screen backend"
which screen >/dev/null && /usr/bin/byobu-select-backend screen
