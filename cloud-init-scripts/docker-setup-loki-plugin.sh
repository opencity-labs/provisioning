#!/bin/bash

set -ex

if docker plugin inspect loki > /dev/null; then
  echo "Loki plugin already installed"
else
  ARCH=$(uname -m)
  if [[ "$ARCH" == "aarch64" ]]; then
    # https://github.com/grafana/loki/pull/9247
    # vecchia versione:
  	# docker plugin install tucksaun/loki-docker-driver:main-arm64 --alias loki --grant-all-permissions
    # ultima arm, citata in https://github.com/grafana/loki/issues/15318
    # ATTENZIONE: non si puo' usare 'latest': usa invece la versione con "-arm64" finale
    docker plugin install grafana/loki-docker-driver:3.3.2-arm64 --alias loki --grant-all-permissions
  else
    # versione ufficiale, usiamo pure :latest, ma va SOLO per x86
    docker plugin install grafana/loki-docker-driver:latest      --alias loki --grant-all-permissions
  fi
fi

if [[ -f /etc/docker/daemon-loki.json ]]; then
  mv -f /etc/docker/daemon-loki.json /etc/docker/daemon.json
  service docker restart
fi
